﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Absolut_Arbeit
{
    public static class LicenseChecker
    {
        public static Boolean CheckLicense(this string input)
        {
            return true;
            //HttpWebRequest reuqest = (HttpWebRequest)WebRequest.Create(new Uri("http://basharbachir.hostingerapp.com/searchData.php") + "?item_name=" + input);
            //reuqest.Method = "GET";
            //String responseString = new StreamReader(((HttpWebResponse)reuqest.GetResponse()).GetResponseStream() ?? throw new InvalidOperationException()).ReadToEnd();
            //return responseString != "[]";
        }

        public static readonly List<Char> X = new List<Char> { '6', 'j', 'e', '7', 'r', '3', 'k', '+', 'z', 'H', '2', 's', 'X', 'P', 'x', 'Y', 'l', 'O', 'o', 'J', 'K', 'D', 'U', '1', 'R', 'a', 'W', 'S', 'Z', 'i', 'C', 'd', 'v', '5', 'F', '9', 'm', 'w', 'y', 'c', 'p', 'G', 'h', '0', 'B', 'b', 'I', 'N', 'n', 't', 'u', 'f', '4', 'g', 'L', '8', '/', 'E', 'M', 'A', 'Q', 'q', 'V', 'T' };

        public static String GetInfos(this String info)
        {
            if (info == null)
            {
                throw new ArgumentNullException("input");
            }
            if (info.Length != 0)
            {
                StringBuilder strb = new StringBuilder();
                foreach (var b in info)
                {
                    strb.Append($"X[{X.IndexOf(b)}],");
                }
                return strb.ToString().Remove(strb.ToString().Length - 1);
            }
            else
            {
                return "Fehler";
            }

        }
    }
}