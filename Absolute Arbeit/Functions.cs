﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;

namespace Absolut_Arbeit
{
    public class Functions
    {
        #region PublicVariables

        private static readonly WebClient wc = new WebClient();
        private static String New_App_Ver = string.Empty;
        public static String Update_Link = "";
        #endregion

        public static Boolean Check_Internet()
        {
            try
            {
                WebRequest request = WebRequest.Create("http://www.gelbeseiten.de/");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response == null || response.StatusCode != HttpStatusCode.OK)
                {
                    return false;
                }
                return true;

            }
            catch (WebException)
            {
                return false;
            }
        }

        public static String Link_Replace(String Link)
        {
            return Link.Replace("ä", "ae").Replace("ü", "ue").Replace("ö", "oe").Replace(" ", "%20");
        }

        public static String Download_source(String url)
        {
            global: try
            {
                String page;

                using (HttpClient client = new HttpClient())
                {
                    using (HttpResponseMessage response = client.GetAsync(url).Result)
                    {
                        using (HttpContent content = response.Content)
                        {
                            page = content.ReadAsStringAsync().Result;
                        }
                    }
                }
                return page;
            }
            catch (HttpRequestException)
            {
                goto global;
            }
            catch (WebException)
            {
                return "Error";
            }
            catch (UriFormatException)
            {
                return "Error";
            }
            catch (AggregateException)
            {
                return "Error";

            }
            catch (ArgumentException)
            {
                return "Error";
            }
        }

        public static async Task<String> GetBetween(String strSource, String strStart, String strEnd)
        {
            if (!strSource.Contains(strStart) || !strSource.Contains(strEnd)) return "";
            Int32 start = strSource.IndexOf(strStart, 0, StringComparison.Ordinal) + strStart.Length;
            Int32 end = strSource.IndexOf(strEnd, start, StringComparison.Ordinal);
            await Task.Delay(0);
            return strSource.Substring(start, end - start);
        }

        public static async Task Update_Check()
        {
            Task<Boolean> task = Task.Run(() => Functions.Check_Internet());
            if (await task)
            {
                try
                {
                    Int16 Version = Convert.ToInt16(Assembly.GetExecutingAssembly().GetName().Version.ToString().Replace(".", ""));

                    String Info = await wc.DownloadStringTaskAsync(new Uri("http://absolut-arbeit.com/AA_Update.txt"));
                    String[] Lines = Info.Split('\n');
                    if (Lines.Length > 0)
                    {
                        for (int i = 0; i < Lines.Length; i++)
                        {
                            if (Lines[i].Contains("msg:"))
                            {
                                MessageBox.Show(Lines[i].Replace("msg:", ""), "Benachrichtigung");
                            }
                            else if (Lines[i].Contains("Ver:"))
                            {
                                Int32 latest_ver = Convert.ToInt32(Lines[i].Replace(".", "").Replace("Ver:", "").Replace(" ", ""));
                                if (latest_ver > Version)
                                {
                                    New_App_Ver = latest_ver.ToString();
                                    if (Lines[i + 1].Contains("Update:"))
                                    {
                                        Update_Link = Lines[i + 1].Replace("Update:", "").Replace(" ", "");
                                    }
                                }
                            }
                            else if (Lines[i].Contains("Stop:"))
                            {
                                MessageBox.Show(Lines[i].Replace("Stop:", ""), "vorübergehend gestoppt");
                                Application.Current.Shutdown();
                            }
                        }
                    }
                }
                catch (IndexOutOfRangeException)
                {
                }
                catch
                {
                    MessageBox.Show("Etwas ist schief gelaufen Nr.017", "Fehler");
                }

            }

        }

        public static async Task Download_Update(String Up)
        {
            try
            {

                if (Up.Contains("Klicken"))
                {
                    String path = string.Empty;
                    MessageBox.Show("Wählen Sie den Pfad zum Speichern des Programms", "Speichern");
                    SaveFileDialog svd = new SaveFileDialog();
                    svd.Filter = "Rar file (*.rar)|*.rar";
                    svd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    if (svd.ShowDialog() == true)
                    {
                        path = svd.FileName;
                        Byte[] data = await wc.DownloadDataTaskAsync(new Uri(Update_Link));
                        if (!path.Contains(".rar"))
                        {
                            path = path + "Absolut Arbeit " + New_App_Ver + ".rar";
                        }
                        File.WriteAllBytes(path, data);
                        MessageBox.Show("Die App wurde gespeichert, im: " + path, "Speichern");
                    }
                    else
                    {
                        MessageBox.Show("Die App wurde nicht gespeichert! ", "Fehler!");
                    }
                }
            }
            catch
            {
                MessageBox.Show("Etwas ist schief gelaufen Nr.84", "Fehler");
            }
        }
    }
}
