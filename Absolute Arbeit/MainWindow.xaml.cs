﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace Absolut_Arbeit
{

    public partial class MainWindow : Window
    {
        private class AddToListView
        {
            public String Email { get; set; }
            public String Site { get; set; }
            public String Name { get; set; }
            public String Adresse { get; set; }
        }

        #region PublicVariables

        private readonly IEnumerable<String> Contact = new String[] { @"/contact.htm", @"/kontakt.html", @"/kontakt", @"/contact", @"/contact.html", @"/en/contact", @"/en/contact.html", @"/service/impressum/", @"/impressum.html", @"/support-de/", @"/company/contact", @"/impressum/", @"/kontakt-und-anfahrt/", @"/kontakt/anfahrt/", @"/conta", @"/contact-us", @"/Hilfe & Kontakt", @"/en/company-contact.html" };
        private readonly IDictionary<Int32, Tuple<String, String, String, String>> Infos = new Dictionary<Int32, Tuple<String, String, String, String>>();
        private readonly static Regex emailRegex = new Regex(@"\b\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private const String key_word = "Detailseite Aktionsleiste Webadresse&quot";
        private static List<AddToListView> details = new List<AddToListView>();
        private readonly List<String> check_notDoubleSite = new List<String>();
        private readonly List<String> SiteDetailsInGelbe = new List<String>();
        private static readonly List<String> All_Links = new List<String>();
        private static List<String> Cities = new List<String>();
        private static List<String> Jobs = new List<String>();
        private static WebClient wc = new WebClient();
        private static Int32 SendChecker { set; get; }
        private static List<String> Emails;
        private Boolean StopTasks = false;
        private static String FirstLink;
        private Boolean lop = false;
        String lin = string.Empty;
        Boolean Entcheck = true;
        private Int32 num = 1;
        private Boolean Deep;
        #endregion

        public MainWindow(Int32 iii, String sss)
        {
            InitializeComponent();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            ServicePointManager.Expect100Continue = true;

            FirstFormDimen();

            Jobs.AddRange(Properties.Resources.Jobs.Split('\n'));
            Jobs.Sort();

            Cities.AddRange(Properties.Resources.Cities.Split('\n'));
            Cities.Sort();

            ComboBox.ItemsSource = Jobs;
            ComboBox2.ItemsSource = Cities;

            slider2.Minimum = 1;
            slider2.Maximum = 3;
            slider2.Value = 3;

            Update();
        }

        private async void Update()
        {
            await Functions.Update_Check();

            if (Functions.Update_Link.Length > 5)
            {
                Updatelbl.Foreground = Brushes.Red;
                Updatelbl.Content = "Es gibt ein neues Update, Klicken Sie hier um es herunterzuladen";

            }
        }

        private async void Button_Click(Object sender, RoutedEventArgs e)
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Absolut");

                key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Absolut", true);
                String data = key.GetValue("ActivationCode").ToString();

                if (data.CheckLicense())
                {
                    String path = TextBox1Path.Text;
                    if (path.Length > 3 && !path.Contains("Fehler"))
                    {
                        Task<Boolean> task = Task.Run(() => Functions.Check_Internet());
                        if (await task)
                        {
                            Butoon.IsEnabled = false;
                            await Load_Data();

                            SeocndFormDimen();

                            Button2.Content = "      Weiter";
                            Button2.IsEnabled = Button32.IsEnabled = ButtonDelete.IsEnabled = ButtonBack.IsEnabled = true;

                            Button3.Content = "Stopp";
                            Button3.IsEnabled = false;
                        }
                        else
                        {
                            MessageBox.Show("Etwas ist schief gelaufen mit den \"Gelbe Seiten\" oder Ihrem Internet. Versuchen Sie es später erneut", "Fehler", MessageBoxButton.OK, MessageBoxImage.Information);
                            Application.Current.Shutdown();
                        }
                    }
                    else
                    {

                        Boolean task = await Task.Run(() => Functions.Check_Internet());
                        if (task)
                        {
                            try
                            {
                                String Job = ComboBox.SelectedItem.ToString().ToLower();
                                String City = ComboBox2.SelectedItem.ToString().ToLower();

                                String Serach_Site = string.Empty;

                                if (!(Boolean)EntCheckBox.IsChecked)
                                {
                                    Serach_Site = @"https://www.gelbeseiten.de/Suche/" + Job + "/" + City;
                                }
                                else
                                {
                                    Serach_Site = @"https://www.gelbeseiten.de/Suche/" + Job + "/" + City + "?umkreis=" + Convert.ToString((Int16)slider3.Value * 1000);
                                }

                                if (CheckBox2.IsChecked == true)
                                {
                                    Deep = true;
                                }
                                else
                                {
                                    Deep = false;
                                }
                                FirstLink = Functions.Link_Replace(Serach_Site);

                                Butoon.IsEnabled = false;
                                Butoon.Content = "Wird geladen";

                                await GetTheLinks();
                                SeocndFormDimen();
                            }
                            catch (NullReferenceException)
                            {
                                MessageBox.Show("Bitte wählen Sie eine Branche oder eine Stadt", "Warnung", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Etwas ist schief gelaufen, mit den \"Gelbe Seiten\" oder Ihrem Internet. Versuchen Sie es später erneut", "Fehler", MessageBoxButton.OK, MessageBoxImage.Information);
                            Application.Current.Shutdown();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Etwas ist schief gelaufen Nr.001", "Fehler");
                }

            }
            catch
            {
                MessageBox.Show("Etwas ist schief gelaufen Nr.074", "Fehler");
            }


        }

        private void LblJob_MouseLeftButtonUp(Object sender, MouseButtonEventArgs e)
        {
            String input = Microsoft.VisualBasic.Interaction.InputBox("Bitte geben Sie Ihren Arbeitsnamen an. (Stellen Sie sicher, dass der Arbeitsname richtig eingegeben wurde)", "Job hinzufügen", "Der Arbeitsname", -1, -1);
            Jobs.Add(input);
            Jobs.Sort();
            ComboBox.ItemsSource = Jobs;
            MessageBox.Show("Arbeitsname wurde hinzugefügt, versuchen Sie es erneut", "Fertig", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Lblcity_MouseLeftButtonUp(Object sender, MouseButtonEventArgs e)
        {
            String input = Microsoft.VisualBasic.Interaction.InputBox("Bitte geben Sie Ihre Stadt an. (Stellen Sie sicher, dass die Stadt richtig eingegeben wurde)", "Stadt hinzufügen", "Stadt", -1, -1);
            Cities.Add(input);
            Cities.Sort();
            ComboBox2.ItemsSource = Cities;
            MessageBox.Show("Die Stadt wurde hinzugefügt, versuchen Sie es erneut", "Fertig", MessageBoxButton.OK, MessageBoxImage.Information);

        }

        private async Task GetTheLinks()
        {
            try
            {
                Decimal All_Ads_numbers = 0;
                Decimal Ads_In_The_Page = 0;

                String b = await Task.Run(() => Functions.Download_source(FirstLink));

                if (!b.Contains("r_total"))
                {
                    MessageBox.Show("Etwas ist schief gelaufen", "!!!");
                    Application.Current.Shutdown();
                }
                else
                {

                    String num = await Functions.GetBetween(b, "utag_data[\"r_total\"] = \"", "\";");
                    All_Ads_numbers = Convert.ToInt32(num);

                    String num2 = await Functions.GetBetween(b, "utag_data[\"r_pghc\"] = \"", "\";");
                    Ads_In_The_Page = Convert.ToInt32(num2);

                    try
                    {
                        if (All_Ads_numbers == 0 || Ads_In_The_Page == 0 || All_Ads_numbers <= 15)
                        {
                            if (b.Contains("GS_trefferliste"))
                            {
                                All_Links.Add(FirstLink);
                            }
                        }
                        else
                        {
                            Decimal All_Pages_Nums = (All_Ads_numbers / Ads_In_The_Page);
                            Int32 All_Pages_Nums_2 = Convert.ToInt32((All_Ads_numbers / Ads_In_The_Page));
                            Int32 All_Pages_Numbers = 0;

                            if (All_Pages_Nums != All_Pages_Nums_2)
                            {
                                All_Pages_Numbers = All_Pages_Nums_2 + 1;
                            }
                            else
                            {
                                All_Pages_Numbers = All_Pages_Nums_2;
                            }
                            All_Links.Add(FirstLink);
                            for (int i = 2; i <= All_Pages_Numbers; i++)
                            {
                                if (!(Boolean)EntCheckBox.IsChecked)
                                {
                                    //All the pages + the links are in the Array =>
                                    All_Links.Add(FirstLink + "/Seite-" + i.ToString());
                                }
                                else
                                {
                                    String umkreis = "?umkreis=" + Convert.ToString((Int16)slider3.Value * 1000);
                                    All_Links.Add(FirstLink.Replace(umkreis, "") + "/Seite-" + i.ToString() + umkreis);
                                }
                            }
                        }

                    }
                    catch (DivideByZeroException)
                    {
                        MessageBox.Show("Es gibt keinen Job", "Fehler");
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Etwas ist schief gelaufen Nr.045" + ex.ToString(), "Fehler!");
            }

        }

        private async Task Get_Details()
        {
            try
            {

                Int32 loop = 0;
                Int32 Links_count = All_Links.Count;
                ListView1.ItemsSource = details;

                //Extract info from Gelbe
                while (loop < Links_count)
                {
                    String src = await Task.Run(() => Functions.Download_source(All_Links[loop]));

                    if (src != "Error")
                    {
                        String[] lines = src.Split('\n');
                        for (int j = 1000; j < lines.Length; j++)
                        {
                            if (lines[j].Contains("class=\"mod mod-Treffer\""))
                            {
                                String Site = await Functions.GetBetween(lines[j + 1], "<a href=\"", "\" data-wipe=");
                                SiteDetailsInGelbe.Add(Site.Replace("https", "http").Replace("href=\"", "").Replace("\"", "").Replace("	", ""));
                            }
                        }
                    }

                    loop++;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                MessageBox.Show("Etwas ist schief gelaufen Nr.51", "Fehler!!");
            }
        }

        private async Task SearchMain()
        {
            Int32 Val = (Int16)slider2.Value;
            if (Val == 1)
            {
                await SlowSearch();
            }
            else if (Val == 2)
            {
                await MiddelSearch();
            }
            else
            {
                await FastSearch();
            }

            if (Infos.Count == 0)
            {
                MessageBox.Show("Es gibt kein Ergebnis!!");
                Button3.IsEnabled = false;
                ButtonBack.IsEnabled = true;
            }
            else
            {
                Button2.Content = "      Weiter";

                Button3.Content = "Stopp";
                Button3.IsEnabled = false;

                Button32.IsEnabled = Button2.IsEnabled = ButtonDelete.IsEnabled = ButtonBack.IsEnabled = true;
            }
        }

        private async Task SlowSearch()
        {
            Int32 Sites = SiteDetailsInGelbe.Count;

            if (Sites > 0)
            {
                Button3.IsEnabled = true;
                await Links_From_Gelbe(0, Sites);
            }
        }

        private async Task MiddelSearch()
        {
            Int32 Sites = SiteDetailsInGelbe.Count;
            List<Task> Tasks = null;

            if (Sites > 0)
            {
                Int32 persent = Sites * 33 / 100;

                Tasks = new List<Task>
                          {
                          Links_From_Gelbe(0, persent),
                          Links_From_Gelbe(persent, persent *2),
                          Links_From_Gelbe(persent *2, Sites)
                          };
                Button3.IsEnabled = true;
                await Task.WhenAll(Tasks);
            }
        }

        private async Task FastSearch()
        {
            try
            {
                Int32 Sites = SiteDetailsInGelbe.Count;
                List<Task> Tasks = new List<Task>();
                if (Sites > 0)
                {
                    if (Sites < 10)
                    {
                        Tasks = new List<Task>
                          {
                          Links_From_Gelbe(0, Sites),
                          };
                    }
                    else
                    {
                        Int32 persent = Sites * 10 / 100;

                        Tasks = new List<Task>
                          {
                        Links_From_Gelbe(0, persent),
                        Links_From_Gelbe(persent, persent * 2),
                        Links_From_Gelbe((persent * 2), persent * 3),
                        Links_From_Gelbe((persent * 3), persent * 4),
                        Links_From_Gelbe((persent * 4), persent * 5),
                        Links_From_Gelbe((persent * 5), persent * 6),
                        Links_From_Gelbe((persent * 6), persent * 7),
                        Links_From_Gelbe((persent * 7), persent * 8),
                        Links_From_Gelbe((persent * 8), persent * 9),
                        Links_From_Gelbe((persent * 9), Sites),
                    };

                    }

                    Button3.IsEnabled = true;
                    await Task.Run(() => Task.WaitAny(Task.WhenAll(Tasks)));
                }

            }
            catch
            {
                MessageBox.Show("Etwas ist schief gelaufen Nr.009", "Fehler");
            }

        }

        private async Task Links_From_Gelbe(Int32 Start, Int32 End)
        {
            try
            {
                for (int i = Start; i < End && !StopTasks; i++)
                {
                    try
                    {
                        String src = await Task.Run(() => Functions.Download_source(SiteDetailsInGelbe[i]));

                        String[] lines = await Task.Run(() => src.Split('\n'));
                        for (int l = 700; l < lines.Length; l++)
                        {

                            String df = lines[l];
                            if (df.Contains(key_word))
                            {
                                String half = df.Substring(df.IndexOf("title=\"") + 7);
                                String website = half.Substring(0, half.IndexOf("\" "));

                                String name = await Functions.GetBetween(src, "<meta name=\"description\" content=\"ᐅ ", " in");
                                if (name.Length > 61)
                                {
                                    name = name.Substring(0, 41);
                                }
                                String add = await Functions.GetBetween(src, "property=\"streetAddress\">", "</span>") + "/" + await Functions.GetBetween(src, "property=\"postalCode\">", "</span>") + "/" + await Functions.GetBetween(src, "\"addressLocality\">", "</span>");
                                await Email_Search(website, name, add);

                                break;
                            }

                        }
                    }
                    catch
                    {
                        MessageBox.Show("Etwas ist schief gelaufen Nr.047", "Fehler");
                    }

                }
            }
            catch
            {
                MessageBox.Show("Etwas ist schief gelaufen Nr.61", "Fehler");
            }
        }

        private async Task Email_Search(String url, String name, String add)
        {
            try
            {
                String src = await Task.Run(() => Functions.Download_source(url).Replace("[at]", "@").Replace("(at)", "@"));

                MatchCollection Matches = await Task.Run(() => emailRegex.Matches(src));

                if (src != "Error")
                {

                    if (Matches.Count < 1)
                    {
                        if (Deep)
                        {
                            foreach (String item in Contact)
                            {
                                String src2 = await Task.Run(() => Functions.Download_source(url + item));

                                if (src2 != "Error")
                                {
                                    //not test yet
                                    MatchCollection emailMatches2 = await Task.Run(() => emailRegex.Matches(src2));

                                    if (emailMatches2.Count > 0)
                                    {
                                        foreach (Object match in emailMatches2)
                                        {
                                            if (match.ToString().Contains("@") && !match.ToString().Contains(".css") && !match.ToString().Contains(".jpg") && !match.ToString().Contains(".png") && !match.ToString().Contains("wixpress") && !match.ToString().Contains("widget") && !match.ToString().Contains(".js"))
                                            {
                                                String email2 = match.ToString();
                                                await Load_To_Ui(email2, url, name, add);
                                                lop = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                if (lop)
                                {
                                    lop = false;
                                    break;
                                }
                            }


                        }
                    }
                    else
                    {
                        foreach (Object item in Matches)
                        {
                            if (!item.ToString().Contains(".jpg") && !item.ToString().Contains(".css") && !item.ToString().Contains(".png") && !item.ToString().Contains("wixpress") && !item.ToString().Contains("widget") && !item.ToString().Contains(".js"))
                            {
                                String email = item.ToString();
                                await Load_To_Ui(email, url, name, add);
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                MessageBox.Show("Etwas ist schief gelaufen Nr.62", "Fehler");
            }
        }

        private async Task Load_To_Ui(String Email, String Site, String Name, String Addrese)
        {
            try
            {
                await ListView1.Dispatcher.InvokeAsync(new Action(delegate ()
                {
                    details.Add(new AddToListView() { Email = Email, Site = Site, Name = Name, Adresse = Addrese });

                    Infos.Add(num, new Tuple<String, String, String, String>(Email, Site, Name, Addrese));
                    num += 1;
                    ListView1.Items.Refresh();
                    label_results.Content = "Ergebnisse: " + (num - 1).ToString();
                }));
            }
            catch
            {
                MessageBox.Show("Etwas ist schief gelaufen Nr.68", "Fehler");
            }
        }

        private void Button2_Click(Object sender, RoutedEventArgs e)
        {
            Button2.IsEnabled = false;

            All_Links.Clear();

            SiteDetailsInGelbe.Clear();

            Emails = (from first in Infos select first.Value.Item1).ToList();
            ListBox1.ItemsSource = Emails;
            TabControl1.SelectedIndex = 2;

            //Infos.Clear();
            this.Width = 1548.349;
            this.Height = 666.325;
        }

        private void Button3_Click(Object sender, RoutedEventArgs e)
        {
            if (!StopTasks)
            {
                StopTasks = true;
                Button3.Content = "Wird gestoppt";
                Button3.IsEnabled = false;
            }

        }

        private void Button4_Click(Object sender, RoutedEventArgs e)
        {
            foreach (var item in ListBox1.SelectedItems)
            {
                String selected = item.ToString();
                if (selected.Length > 3)
                {
                    Emails.Remove(selected);
                }
            }
            ListBox1.Items.Refresh();
        }

        private async void Button5_Click(Object sender, RoutedEventArgs e)
        {
            try
            {

                if (!TextBox2.Text.Contains("@outlook") || Textbox3.Password.Length <= 4)
                {
                    MessageBox.Show("überprüfen Sie den Benutzernamen oder das Passwort erneut", "Fehler", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    Boolean task = await Task.Run(() => Functions.Check_Internet());
                    if (task)
                    {

                        for (int j = 0; j < ListBox1.Items.Count; j++)
                        {
                            //From Address
                            //string FromAddress = TextBox2.Text;
                            //string FromAdressTitle = Textbox3.Password;

                            ////To Address
                            //string ToAddress = "loaialkhateeb@gmail.com";
                            //string ToAdressTitle = "Microsoft ASP.NET Core";
                            //string Subject = "Hello World - Sending email using ASP.NET Core 1.1";
                            //string BodyContent = "ASP.NET Core was previously called ASP.NET 5. It was renamed in January 2016. It supports cross-platform frameworks ( Windows, Linux, Mac ) for building modern cloud-based internet-connected applications like IOT, web apps, and mobile back-end.";

                            ////Smtp Server
                            //string SmtpServer = "smtp.live.com";
                            ////Smtp Port Number
                            //int SmtpPortNumber = 587;

                           
                        }

                        MessageBox.Show(
                            SendChecker == 0
                                ? $"{0} E-Mails wurden gesandet"
                                : $"{ListBox1.Items.Count} E-Mails wurde gesandet", "erfolgreich gesendet",
                            MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Etwas ist schief gelaufen mit den \"Gelbe Seiten\" oder Ihrem Internet. Versuchen Sie es später erneut", "Fehler", MessageBoxButton.OK, MessageBoxImage.Information);
                        Application.Current.Shutdown();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                MessageBox.Show("Etwas ist schief gelaufen Nr.31", "Fehler");
            }
        }

        private void Button6_Click(Object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                String Path = ofd.FileName;
                ListBox2.Items.Add(Path);
            }
        }

        private void Button7_Click(Object sender, RoutedEventArgs e)
        {
            try
            {
                String selected = ListBox2.SelectedValue.ToString();

                for (int i = 0; i < ListBox2.Items.Count; i++)
                {
                    if (ListBox2.Items[i].ToString() == selected)
                    {
                        ListBox2.Items.RemoveAt(i);
                    }
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Sie müssen eine Datei auswählen", "Fehler");
            }
        }

        private void Slider2_ValueChanged(Object sender, RoutedPropertyChangedEventArgs<Double> e)
        {
            Int32 val = (Int32)slider2.Value;
            if (val == 1)
            {
                LableSpeed.Content = "Langsam";

            }
            else if (val == 2)
            {
                LableSpeed.Content = "Mittel";

            }
            else
            {
                LableSpeed.Content = "Schnell";

            }

        }

        private void ButtonPath_Click(Object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == true)
            {
                String Path = ofd.FileName;
                if (Path.Contains(".txt"))
                {
                    TextBox1Path.Text = Path;
                }
                else
                {
                    MessageBox.Show("Der Datentyp sollte .txt sein", "Fehler", MessageBoxButton.OK, MessageBoxImage.Information);
                    TextBox1Path.Text = "Fehler";
                }
            }
        }

        private void Button3_2(Object sender, RoutedEventArgs e)
        {
            ExportForm ex = new ExportForm(Infos);
            ex.Show();
        }

        private async Task Load_Data()
        {
            try
            {
                await Task.Delay(0);
                String path = TextBox1Path.Text;
                String[] lines = File.ReadAllLines(path);
                if (lines[0].Length == 4)
                {
                    for (int i = 2; i < lines.Length; i++)
                    {
                        if (lines[i].Contains(","))
                        {
                            String[] lineSplit = lines[i].Split(',');
                            Infos.Add(num, new Tuple<String, String, String, String>(lineSplit[0], lineSplit[1], lineSplit[2], lineSplit[3]));
                            num++;
                        }
                    }

                    ListView1.ItemsSource = details;

                    foreach (var item in Infos)
                    {
                        details.Add(new AddToListView() { Email = item.Value.Item1, Site = item.Value.Item2, Name = item.Value.Item3, Adresse = item.Value.Item4 });
                    }
                    ListView1.Items.Refresh();

                    label_results.Content = "Ergebnisse: " + ListView1.Items.Count.ToString();
                }
                else
                {
                    MessageBox.Show("Etwas ist schief gelaufen mit der geladenen Dateien", "Fehler");
                    Application.Current.Shutdown();
                }
            }
            catch
            {
                MessageBox.Show("Etwas ist schief gelaufen mit der geladenen Dateien", "Fehler");
                Application.Current.Shutdown();
            }

        }

        private void Form1_Closing(Object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Slider3_ValueChanged(Object sender, RoutedPropertyChangedEventArgs<Double> e)
        {
            slider3.Minimum = 0;
            slider3.Maximum = 50;
            SliderLbl.Content = (Int16)slider3.Value + " km";

        }

        private void EntCheckBox_Click(Object sender, RoutedEventArgs e)
        {
            if (Entcheck)
            {
                slider3.IsEnabled = true;
                Entcheck = false;
            }
            else
            {
                slider3.IsEnabled = false;
                Entcheck = true;
                SliderLbl.Content = "km";
                slider3.Value = 0;
            }
        }

        private async void Update_btn_Click(Object sender, RoutedEventArgs e)
        {
            await Functions.Download_Update(Updatelbl.Content.ToString());
        }

        private void Kontakt_Btn_Click(Object sender, RoutedEventArgs e)
        {
            About A = new About();
            A.Show();
        }

        private void ButtonDelete_Click(Object sender, RoutedEventArgs e)
        {
            Int32 index = ListView1.SelectedIndex;
            if (index >= 0)
            {
                details.RemoveAt(index);
                ListView1.Items.Refresh();
                label_results.Content = "Ergebnisse: " + ListView1.Items.Count.ToString();
            }
            else
            {
                MessageBox.Show("Sie haben nichts ausgewählt", "Warnung", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FirstFormDimen()
        {
            TabControl1.SelectedIndex = 0;
            this.Width = 727;
            this.Height = 607;
            Butoon.IsEnabled = true;
            Butoon.Content = "Weiter";
            ButtonBack.IsEnabled = ButtonDelete.IsEnabled = Button2.IsEnabled = Button32.IsEnabled = Button3.IsEnabled = false;

            if (Infos.Count > 1)
            {
                Reset();
            }
        }

        private void SeocndFormDimen()
        {
            TabControl1.SelectedIndex = 1;
            this.Height = 490;
            this.Width = 1730;
            this.Left = SystemParameters.PrimaryScreenWidth / 15;
        }

        private void Reset()
        {
            label_results.Content = "";
            details.Clear();
            Infos.Clear();
            check_notDoubleSite.Clear();
            SiteDetailsInGelbe.Clear();
            All_Links.Clear();
            FirstLink = string.Empty;
            lop = StopTasks = false;
            num = 1;
            TextBox1Path.Text = "";
            Button3.Content = "Stopp";

        }

        private void ButtonBack_Click(Object sender, RoutedEventArgs e)
        {
            FirstFormDimen();
        }

        private void ListView1_MouseLeftButtonUp(Object sender, MouseButtonEventArgs e)
        {
            try
            {
                Int32 index = ListView1.SelectedIndex;
                String site = details[index].Site.Replace(" ", "");
                if (lin == site)
                {
                    Process.Start(site);
                }
                else
                {
                    lin = site;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private async void TabControl1_SelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            if (TabControl1.SelectedIndex == 1)
            {
                await Get_Details();
                await SearchMain();
            }
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
         
        }
    }
}
