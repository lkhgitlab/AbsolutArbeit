﻿using System.Windows;

namespace Absolut_Arbeit
{
    public partial class About : Window
    {
        public About()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://absolut-arbeit.com");
        }
    }
}
