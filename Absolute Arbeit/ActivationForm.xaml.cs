﻿using Microsoft.Win32;
using System;
using System.IO;
using System.IO.Compression;
using System.Management;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows;

namespace Absolut_Arbeit
{
    /// <summary>
    /// Interaction logic for ActivationForm.xaml
    /// </summary>
    public partial class ActivationForm : Window
    {

        private static String Model { set; get; }
        private static String Serial { set; get; }
        private static readonly String JsonLicenseIfos = "{\r\n  \"Liecense-Requirements\": {\r\n    \"X1\": \"x1\",\r\n    \"X2\": \"x2\",\r\n    \"X3\": \"x3\",\r\n    \"X4\": \"x4\",\r\n    \"X5\": \"x5\",\r\n    \"X6\": \"x6\",\r\n  },\r\n}";


        public ActivationForm()
        {
            InitializeComponent();

            if (!Check_Internet())
            {
                RegCheck();
            }
            else
            {
                MessageBox.Show("Es gibt ein Problem mit dem Internet", "Fehler");
                Application.Current.Shutdown();
            }
        }

        private static Boolean Check_Internet()
        {
            try
            {
                WebRequest request = WebRequest.Create("http://basharbachir.hostingerapp.com/searchData.php");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                if (response == null || response.StatusCode != HttpStatusCode.OK)
                {
                    return false;
                }
                return true;

            }
            catch (WebException)
            {
                return false;
            }
        }

        public void RegCheck()
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Absolut");
                Int32 ValTrue = 0;

                if (key != null)
                {
                    ValTrue = Array.IndexOf(key.GetValueNames(), "ActivationCode");
                }

                if (key == null || ValTrue != 0 )
                {
                    SetUp();
                }
                else
                {
                    try
                    {
                        key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Absolut", true);
                        String data = key.GetValue("ActivationCode").ToString();
                        using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive"))
                        {
                            foreach (ManagementBaseObject o in searcher.Get())
                            {
                                ManagementObject info = (ManagementObject)o;

                                Model = info["Model"].ToString().Replace(" ", "");
                                Serial = info["SerialNumber"].ToString().Replace(" ", "");
                            }
                        }
                        key.Close();
                        AES_Decrypt(data, Model, Serial);
                    }
                    catch
                    {
                        MessageBox.Show("Etwas ist schief gelaufen Nr.005", "Fehler");
                        Application.Current.Shutdown();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Etwas ist schief gelaufen Nr.021", "Fehler");
                Application.Current.Shutdown();
            }
        }

        private void SetUp()
        {
            try
            {
                Boolean check = true;

                textbox1.Text = Environment.MachineName.GetInfos();
                textbox2.Text = Environment.OSVersion.ToString().GetInfos();
                using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive"))
                {
                    foreach (var o in searcher.Get())
                    {
                        var info = (ManagementObject)o;
                        textbox3.Text = info["DeviceID"].ToString().Replace(" ", "").GetInfos();
                        textbox4.Text = info["Model"].ToString().Replace(" ", "").GetInfos();
                        Model = info["Model"].ToString().Replace(" ", "");
                        textbox5.Text = info["InterfaceType"].ToString().Replace(" ", "").GetInfos();
                        try
                        {
                            textbox6.Text = info["SerialNumber"].ToString().Replace(" ", "").GetInfos();
                            Serial = info["SerialNumber"].ToString().Replace(" ", "");
                        }
                        catch
                        {
                            MessageBox.Show("Es gibt keine Serial-Nummer", "Fehler");
                            check = false;
                        }
                    }
                }

                if (check)
                {
                    Button1.IsEnabled = true;
                    File.WriteAllText("Liecense-Requirements.json", JsonLicenseIfos.Replace("x1", textbox1.Text).Replace("x2", textbox2.Text).Replace("x3", textbox3.Text).Replace("x4", textbox4.Text).Replace("x5", textbox5.Text).Replace("x6", textbox6.Text));
                    MessageBox.Show("Bitte senden Sie uns die Datei, die neben dem Programm unter dem Namen Liecense-Requirements.json erstellt wurde", "Wichtig!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    Application.Current.Shutdown();
                }
            }
            catch
            {
                MessageBox.Show("Etwas ist schief gelaufen Nr.052", "Fehler");
            }
        }

        private void Button_Click(Object sender, RoutedEventArgs e)
        {
            if (textbox7.Text.Length > 5)
            {
                if (textbox7.Text.CheckLicense())
                {
                    AES_Decrypt(textbox7.Text, Model, Serial);
                }
                else
                {
                    MessageBox.Show("Etwas ist schief gelaufen Nr.005", "Fehler");
                }
            }
            else
            {
                MessageBox.Show("Falsch Code", "Fehler");
            }

        }

        public void AES_Decrypt(String K1, String P1, String pass5)
        {
            try
            {
                Byte[] bytes = StringToByteArray(K1);
                String org;
                using (var msi = new MemoryStream(bytes))
                {
                    using (var mso = new MemoryStream())
                    {
                        using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                        {
                            gs.CopyTo(mso);
                        }
                        org = Encoding.Unicode.GetString(mso.ToArray());
                    }
                }
                Byte[] bytesToBeDecrypted = Convert.FromBase64String(org);
                Byte[] passwordBytes = Encoding.UTF8.GetBytes(P1);
                passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

                Byte[] decryptedBytes = null;
                if (decryptedBytes == null)
                {
                    Byte[] saltBytes = new Byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (RijndaelManaged AES = new RijndaelManaged())
                        {
                            AES.KeySize = 256;
                            AES.BlockSize = 128;

                            using (Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000))
                            {
                                AES.Key = key.GetBytes(AES.KeySize / 8);
                                AES.IV = key.GetBytes(AES.BlockSize / 8);
                            }

                            AES.Mode = CipherMode.CBC;
                            if (AES.Mode == CipherMode.CBC)
                            {
                                using (CryptoStream cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                                {
                                    cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                                    cs.Close();
                                }
                                decryptedBytes = ms.ToArray();
                            }
                            else
                            {
                                Application.Current.Shutdown();
                            }

                        }
                    }
                    String result = Encoding.UTF8.GetString(decryptedBytes).Replace(" ", "");
                    if (result.Length > 1)
                    {
                        String neww = "";
                        using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive"))
                        {
                            foreach (ManagementObject info in searcher.Get())
                            {

                                pass5 = info["SerialNumber"].ToString().Replace(" ", "");
                            }
                        }
                        neww = pass5;
                        if (result.Length != pass5.Length)
                        {
                            Application.Current.Shutdown();
                        }
                        else
                        {
                            if (BitConverter.ToString(Encoding.Default.GetBytes(result)) == BitConverter.ToString(Encoding.Default.GetBytes(neww)))
                            {

                                this.Hide();
                                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\", true);
                                key.CreateSubKey("Absolut");
                                key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Absolut", true);
                                key.SetValue("ActivationCode", K1);
                                key.Close();
                                var key2 = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Absolut", true);
                                String data = key2.GetValue("ActivationCode").ToString();
                                if (data.CheckLicense())
                                {
                                    MainWindow M = new MainWindow(55,"AKKSS");
                                    M.Show();
                                }
                                else
                                {
                                    MessageBox.Show("Etwas ist schief gelaufen Nr.005", "Fehler");
                                    Application.Current.Shutdown();
                                }

                            }
                            else
                            {
                                Application.Current.Shutdown();
                            }
                        }
                    }
                    else
                    {
                        Application.Current.Shutdown();

                    }
                }
                else
                {
                    Application.Current.Shutdown();
                }
            }
            catch
            {
                MessageBox.Show("Etwas ist schief gelaufen Nr.055 oder Falsch Code", "Fehler!");
                Application.Current.Shutdown();
            }

        }

        public static Byte[] StringToByteArray(String hex)
        {
            if (hex == null)
            {
                throw new ArgumentNullException("hex");
            }
            if (hex.Length != 0)
            {
                int NumberChars = hex.Length;
                byte[] bytes = new byte[NumberChars / 2];
                for (int i = 0; i < NumberChars; i += 2)
                    bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
                return bytes;
            }
            else
            {
                return new Byte[] { 0 };
            }

        }

    }
}
