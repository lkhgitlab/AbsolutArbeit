﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace Absolut_Arbeit
{
    /// <summary>
    /// Interaction logic for ExportForm.xaml
    /// </summary>
    public partial class ExportForm : Window
    {

        private IDictionary<Int32, Tuple<String, String, String, String>> Infos = new Dictionary<Int32, Tuple<String, String, String, String>>();

        public ExportForm(IDictionary<Int32, Tuple<String, String, String, String>> all)
        {
            InitializeComponent();
            Infos = all;
        }

        private void Button1_Click(Object sender, RoutedEventArgs e)
        {
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (!string.IsNullOrEmpty(fbd.SelectedPath) && result == System.Windows.Forms.DialogResult.OK)
                {
                    String Path = fbd.SelectedPath.ToString();
                    TextBox1.Text = Path;
                }
            }
        }

        private async void Button2_Click(Object sender, RoutedEventArgs e)
        {
            String path = TextBox1.Text;
            if (Check2.IsChecked == true || Check3.IsChecked == true || Check4.IsChecked == true || Check5.IsChecked == true)
            {
                if (!string.IsNullOrEmpty(path) && path.Length > 3)
                {
                    await Export();
                    System.Windows.MessageBox.Show("Die Daten wurden erfolgreich exportiert", "Erfolgreich");
                    Process.Start(TextBox1.Text);
                }
                else
                {
                    System.Windows.MessageBox.Show("Bitte wählen Sie einen Ordner zum Speichern der Daten", "Fehler", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Bitte wählen Sie eine Option zum Exportieren", "Fehler", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private async Task Export()
        {
            await Task.Delay(0);
            StringBuilder SB = new StringBuilder();

            SB.Append(Check2.IsChecked == true ? "1" : "0");

            SB.Append(Check3.IsChecked == true ? "1" : "0");

            SB.Append(Check4.IsChecked == true ? "1" : "0");

            SB.Append(Check5.IsChecked == true ? "1" : "0");

            SB.AppendLine();
            SB.AppendLine("Email, Site, Name, Address");

            foreach (var item in Infos)
            {
                SB.AppendLine();

                if (Check2.IsChecked == true)
                {
                    SB.Append(item.Value.Item1 + ", ");
                }
                else
                {
                    SB.Append("" + ", ");
                }

                if (Check3.IsChecked == true)
                {
                    SB.Append(item.Value.Item2 + ", ");
                }
                else
                {
                    SB.Append("" + ", ");
                }

                if (Check4.IsChecked == true)
                {
                    SB.Append(item.Value.Item3 + ", ");
                }
                else
                {
                    SB.Append("" + ", ");
                }

                if (Check5.IsChecked == true)
                {
                    SB.Append(item.Value.Item4 + "");
                }
                else
                {
                    SB.Append("" + "");
                }
            }
            System.IO.File.WriteAllText(TextBox1.Text + @"\Contacts.txt", SB.ToString());
        }

        private static Boolean checkk = true;

        private void Check1_Click(Object sender, RoutedEventArgs e)
        {
            if (checkk)
            {
                Check2.IsChecked = true;
                Check3.IsChecked = true;
                Check4.IsChecked = true;
                Check5.IsChecked = true;
                checkk = false;
            }
            else
            {
                Check2.IsChecked = false;
                Check3.IsChecked = false;
                Check4.IsChecked = false;
                Check5.IsChecked = false;
                checkk = true;
            }

        }

    }
}
